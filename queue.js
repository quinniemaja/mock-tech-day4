let collection = [];

// Write the queue functions below.

// Output all the elements of the queue
function print() {
  	console.log(collection);
  	return collection;
}

// Adds element to the rear of the queue
function enqueue(element) {
    collection.push(element);
    console.log(collection)
    return collection;
}


// Removes element from the front of the queue
function dequeue(element) {
  collection.shift(element);
  console.log(collection);
  return collection;

}

// Show element at the front
function front() {
	console.log(collection[0]);
	return collection[0];
  
}

// Show total number of elements
function size() {
   console.log(collection.length);
   return collection.length;
}

// Outputs a Boolean value describing whether queue is empty or not
function isEmpty() {
    for(let i=0; i <= collection.length-1; i++) {
    	if(collection[i] === null) {
    		return true
    	} else {
    		return false
    	}
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
